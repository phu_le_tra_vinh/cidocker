`date: Apr 27 2017`

`writer: PhuLeTV`

# About this repo
Đây là repo chứ các Dockfile, dùng cho việc develop các dự án LAMP, LEMP, trong tương lai sẽ cập nhật thêm wordpress, symphony và các framwork phổ biến của php, nodejs. Trong image sẽ chứa toàn bộ các phần mềm php kể cả mysql, mục đích là để start container lên làm việc nhanh nhất, container chứa allinone, để biết thêm có thể vào xem các Dockerfile.


## Requirement:

- PC phải dùng Windows 10 full update.
- Nếu có cài Virtualbox hay VMWare, thì phải tự stop service. Không thể chạy Hyper-V cùng với các phần mềm này.
- Có sẵn hyper-v. Nếu chưa có, mở powershell với administrator chạy lệnh (chỉ chạy trên Win 8/10):

`enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All`

- Install Docker for Windows, thời điểm hiện tại là Docker for Windows v17.03
- Cài đặt gitbash, putty (ssh client), winscp (SFTP client).


## Sử dụng:

ví dụ cho LAMP5.6. Có 2 cách để chạy container:

### 1. Tự tạo image

**Cách 1:** Tự build image từ Dockerfile, tạo ra image cho riêng mình tạm đặt tên là project001
`cd LAMPhp5.6`

`docker build -t project001 .`

**Cách 2:** dùng image build sẵn trên dockerhub, đã được build từ Dockerfile này (khuyến khích):

`docker pull phulerock/lamphp5.6`

Liệt kê danh sách image đang có trên máy:

`docker images`

### 2. Run container
start container từ images này, đặt tên là devmachine, mount thư mục webapp chứ code vào container, NAT ra ngoài port 8080 cho web, port 3306 cho mysql, 2202 cho ssh:

`docker run --name devmachine -p 8080:80 -p 3306:3306 -p 2202:22 -d project001`

Dùng chrome: http://localhost:8080/test.php

`telnet localhost 3306`

### 3. Làm việc với container:
Thư mục webapp chứa code.
Nếu muốn ssh vào trong container, thì dùng key nonpass.rsa (cho linux), nonpass.ppk (cho putty)

`cd LAMPhp5.6`

`ssh -i .\nonpass.rsa -p 2202  root@localhost`